FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7
COPY ./server.py /app/server.py 
COPY  ./requirements.txt /app/requirements.txt
COPY ./templates /app/templates 
COPY ./static /app/static/
WORKDIR /app
RUN pip install -r requirements.txt 
ENTRYPOINT [ "python" ]
CMD [ "server.py" ]
